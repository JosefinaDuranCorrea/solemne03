/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.solemne03j.services;


import com.pruebaa03.Entity.Libros;
import com.pruebaa03.dao.LibrosJpaController;
import com.pruebaa03.dao.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("libros")
public class librosRest {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarlibros(){
      
     LibrosJpaController dao= new LibrosJpaController();
     
     List<Libros> libros=dao.findLibrosEntities();
     return Response.ok(200).entity(libros).build();
    
    }
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response Crear(Libros libro){
      LibrosJpaController dao= new LibrosJpaController();
        try {
            dao.create(libro);
        } catch (Exception ex) {
            Logger.getLogger(librosRest.class.getName()).log(Level.SEVERE, null, ex);
        }
      
      return Response.ok(200).entity(libro).build();
  }
  @PUT
  @Produces(MediaType.APPLICATION_JSON)
 public Response Actualizar(Libros libro){ 
      LibrosJpaController dao= new LibrosJpaController();
        try {
            dao.edit(libro);
        } catch (Exception ex) {
            Logger.getLogger(librosRest.class.getName()).log(Level.SEVERE, null, ex);
        }
      return Response.ok(200).entity(libro).build();
     
 }
 @DELETE
 @Path("/{ideliminar}")
 @Produces(MediaType.APPLICATION_JSON)
  public Response eliminar(@PathParam("ideliminar") String ideliminar){
      LibrosJpaController dao= new LibrosJpaController();
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(librosRest.class.getName()).log(Level.SEVERE, null, ex);
        }
      
      return Response.ok("libro eliminado").build();
      
      
  }
  @GET
 @Path("/{idconsultar}")
 @Produces(MediaType.APPLICATION_JSON)
 public Response consultarPorId (@PathParam("idconsultar") String idconsultar){
     LibrosJpaController dao= new LibrosJpaController();
     Libros libro=dao.findLibros(idconsultar);
     
     return Response.ok(200).entity(libro).build();
     
 }
}
