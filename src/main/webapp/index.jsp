<%-- 
    Document   : index
    Created on : 26/06/2021, 10:31:07 PM
    Author     : Josefina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page.</title>
    </head>
    <body>
        <h1>Endpoints Api Libros</h1>
       
<div class="p-3 mb-2 bg-info text-white">Lista Libros  - GET - https://solemnee03.herokuapp.com/api/libros</div>
<div class="p-3 mb-2 bg-secondary text-white">Lista Libros por ID - GET - https://solemnee03.herokuapp.com/api/libros/{id}</div>
<div class="p-3 mb-2 bg-success text-white">Crear Libros - POST - https://solemnee03.herokuapp.com/api/libros</div>
<div class="p-3 mb-2 bg-danger text-white">Actualizar Libros - PUT - https://solemnee03.herokuapp.com/api/libros</div>
<div class="p-3 mb-2 bg-warning text-dark">Eliminar Libros - DELETE - https://solemnee03.herokuapp.com/api/libros/{id}</div>

    </body>
</html>
